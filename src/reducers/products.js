import { createSlice } from '@reduxjs/toolkit';

export const productsSlice = createSlice({
    name: "products",
    initialState: {
        productList: [],
        cart: []
    },
    reducers: {
        setProducts: (state, { payload }) => {
            state.productList = payload;
        },
        addToCart: (state, { payload }) => {
            const cart = state.cart;
            const cartItem = cart.filter((item) => item.productId === payload.productId);
            if (cartItem.length <= 0) {
                state.cart = [...state.cart, payload];
            } else {
                cartItem[0].qty += 1;
            }
        },
        adjustQty: (state, { payload }) => {
            state.cart.forEach((item) => {
                if (item.productId === payload) {
                    item.qty += 1;
                }
            });
        },
        removeQty: (state, { payload }) => {
            state.cart.forEach((item) => {
                if (item.productId === payload) {
                    item.qty -= 1;
                }
            });
        },
        removeItemCart: (state, { payload }) => {
            let itemCarts = state.cart.filter(item => item.productId !== payload);
            state.cart = itemCarts;
        },
        clearCart: state => {
            state.cart = [];
        }
    }
});

export const { setProducts, addToCart, adjustQty, removeQty, removeItemCart, clearCart } = productsSlice.actions;

export const selectProductList = state => state.products.productList;
export const selectCart = state => state.products.cart;
export const selectQtyItemCart = ((state) => {
    const cart = state.products.cart;
    var count = 0;
    cart.filter((item) => {
        count += item.qty;
        return item;
    });
    return count;
});

export default productsSlice.reducer;