import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setProducts } from './reducers/products';
import './App.css';
import { Header, ContainerBox, Cart, SummaryOrder, Success } from './components';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  const dispatch = useDispatch();
  const [productList, setProductList] = useState([]);


  useEffect(() => {
    fetch("http://localhost:3001/api/getProducts", {
      "method": "GET",
      "headers": {
        "content-type": "application/json",
        "accept": "application/json"
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (response.success) {
          dispatch(setProducts(response.data));
          setProductList(response.data);
        } else {
          alert(JSON.stringify(response.error));
        }
      })
      .catch(err => {
        console.log(err);
      });
  }, [dispatch]);

  return (
    <Router>
      <Switch>
        <React.Fragment>
          <Header />
          <Route exact path="/">
            <ContainerBox productList={productList} />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/summary">
            <SummaryOrder />
          </Route>
          <Route path="/success">
            <Success />
          </Route>
        </React.Fragment>
      </Switch>
    </Router>
  );
}

export default App;
