import { configureStore } from '@reduxjs/toolkit';
import products from '../reducers/products';

export default configureStore({
    reducer: {
        products
    }
});