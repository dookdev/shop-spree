import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import NumberFormat from 'react-number-format';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, selectCart } from '../../reducers/products';
import './ContainerBox.css';
import emptyLogo from '../../assets/icons/empty.png';

export default function ContainerBox({ productList }) {
    const dispatch = useDispatch();
    const [products, setProduct] = useState([]);
    const cart = useSelector(selectCart);

    useEffect(() => {
        setProduct(productList);
    }, [productList]);

    function addProductToCart(product) {
        var isMaxQty = false;
        cart.filter((item) => {
            if (item.productId === product.id) {
                if (item.qty >= product.quantity) {
                    isMaxQty = true;
                }
            }
            return item;
        });

        if (isMaxQty) {
            alert("Out of Stock!");
        } else {
            const item = {
                productId: product.id,
                qty: 1
            };
            dispatch(addToCart(item));
        }
    }

    var CardList;

    if (products.length > 0) {
        CardList = products.map((item, i) => {
            return (
                <Col xs={6} sm={6} md={4} lg={3} xl={2} key={i} className="col-pd">
                    <Card className="card">
                        <Card.Img variant="top" src={item.picture} />
                        <Card.Body>
                            <Card.Title className="pd-name" title={item.name}>{item.name}</Card.Title>
                            <div className="price-ct">
                                <div className="price-txt">
                                    <NumberFormat
                                        decimalScale={2}
                                        decimalSeparator="."
                                        fixedDecimalScale
                                        prefix="฿"
                                        thousandSeparator={true}
                                        displayType={'text'}
                                        value={item.price}
                                    />
                                </div>
                                <div>Quantity: <span className={item.quantity <= 0 ? "qty-0" : "qty"}>{item.quantity}</span></div>
                            </div>
                            <div className="btn-cart-box">
                                <Button onClick={() => { addProductToCart(item) }} variant={item.quantity <= 0 ? "danger" : "outline-primary"} disabled={item.quantity <= 0 ? true : false}>{item.quantity <= 0 ? "Sold Out" : "Add to Cart"}</Button>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            );
        });

        return (
            <Container fluid className="ct-bg">
                <Row>
                    {CardList}
                </Row>
            </Container>
        )
    } else {
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} sm={12} md={1} lg={2} xl={2}></Col>
                    <Col xs={12} sm={12} md={10} lg={8} xl={7} className="empty">
                        <div className="center-screen">
                            <img src={emptyLogo} alt="empty" width="256px" height="256px" />
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={1} lg={2} xl={3}></Col>
                </Row>
            </React.Fragment>
        );
    }
}
