import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectCart, selectProductList, removeItemCart, adjustQty, removeQty } from '../../reducers/products';
import { Col, Row, Media, Button } from 'react-bootstrap';
import NumberFormat from 'react-number-format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import './Cart.css';
import cartLogo from '../../assets/icons/empty-cart.png';
import { useHistory } from 'react-router-dom';

export default function Cart() {
    let history = useHistory();
    const dispatch = useDispatch();
    const cart = useSelector(selectCart);
    const productList = useSelector(selectProductList);
    const [items, setItems] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);
    const [vat, setVat] = useState(0);
    const [net, setNet] = useState(0);

    useEffect(() => {
        let itemList = [];
        cart.forEach(el => {
            productList.forEach(product => {
                if (product.id === el.productId) {
                    product = { ...product, qty: el.qty };
                    itemList.push(product);
                }
            });
        });
        let _total = 0;
        let _vat = 0;
        let _net = 0;

        itemList.forEach((item) => {
            _total += (item.price * item.qty);
        });
        _vat = _total * 0.07;
        _net = _total + _vat;
        setTotalPrice(_total);
        setVat(_vat);
        setNet(_net);
        setItems(itemList);
    }, [cart, productList]);

    function onRemovItemCart(productId) {
        dispatch(removeItemCart(productId));
    }

    function onRemoveQty({ id }) {
        var isLessthanOne = false;
        cart.filter((item) => {
            if (item.productId === id) {
                if (item.qty <= 1) {
                    isLessthanOne = true;
                }
            }
            return item;
        });
        if (isLessthanOne) {
            alert("can not remove qty item less than 1!");
        } else {
            dispatch(removeQty(id));
        }
    }

    function onAdjustQty({ id, quantity }) {
        var isMaxQty = false;
        cart.filter((item) => {
            if (item.productId === id) {
                if (item.qty >= quantity) {
                    isMaxQty = true;
                }
            }
            return item;
        });
        if (isMaxQty) {
            alert("Out of Stock!");
        } else {
            dispatch(adjustQty(id));
        }
    }

    function goToSummaryOder(path) {
        history.push(path);
    }

    if (items.length > 0) {
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} sm={12} md={1} lg={2} xl={2}></Col>
                    <Col xs={12} sm={12} md={10} lg={8} xl={7} className="cart-box">
                        {
                            items.map((item, i) => {
                                return (
                                    <ul className="list-unstyled" key={i}>
                                        <Media as="li">
                                            <img
                                                width={128}
                                                height={128}
                                                className="mr-3"
                                                src={item.picture}
                                                alt="Generic placeholder"
                                            />
                                            <Media.Body className="m-box">
                                                <h5 className="pd-name" title={item.name}>{item.name}</h5>
                                                <p className="price-txt">
                                                    <NumberFormat
                                                        decimalScale={2}
                                                        decimalSeparator="."
                                                        fixedDecimalScale
                                                        prefix="฿"
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        value={item.price}
                                                    />
                                                </p>
                                                <div className="cart-act-layout">
                                                    <div><span className="btn-rm" onClick={() => { onRemoveQty(item) }}>- </span> {item.qty} <span className="btn-add" onClick={() => { onAdjustQty(item) }}> +</span></div>
                                                    <div><FontAwesomeIcon icon={faTrashAlt} size="lg" className="remove-icon" title="Delete" onClick={() => { onRemovItemCart(item.id) }} /></div>
                                                </div>
                                            </Media.Body>
                                        </Media>
                                    </ul>
                                );
                            })

                        }
                    </Col>
                    <Col xs={12} sm={12} md={1} lg={2} xl={3}>
                        <div className={items.length <= 0 ? "display-none" : "cart-box"}>
                            <h5>Summary</h5>
                            <div className="summary-box">
                                <div>Total:</div>
                                <div>
                                    <NumberFormat
                                        decimalScale={2}
                                        decimalSeparator="."
                                        fixedDecimalScale
                                        prefix="฿"
                                        thousandSeparator={true}
                                        displayType={'text'}
                                        value={totalPrice}
                                    />
                                </div>
                            </div>
                            <div className="summary-box">
                                <div>Vat 7%:</div>
                                <div>
                                    <NumberFormat
                                        decimalScale={2}
                                        decimalSeparator="."
                                        fixedDecimalScale
                                        prefix="฿"
                                        thousandSeparator={true}
                                        displayType={'text'}
                                        value={vat}
                                    />
                                </div>
                            </div>
                            <div className="summary-box">
                                <div>Net:</div>
                                <div>
                                    <NumberFormat
                                        decimalScale={2}
                                        decimalSeparator="."
                                        fixedDecimalScale
                                        prefix="฿"
                                        thousandSeparator={true}
                                        displayType={'text'}
                                        value={net}
                                    />
                                </div>
                            </div>
                            <div className="checkout-box">
                                <Button variant="success" onClick={() => { goToSummaryOder("/summary") }}>Checkout</Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </React.Fragment >
        )
    } else {
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} sm={12} md={1} lg={2} xl={2}></Col>
                    <Col xs={12} sm={12} md={10} lg={8} xl={7} className="cart-empty">
                        <div className="center-screen">
                            <img src={cartLogo} alt="empty cart" width="256px" height="256px" />
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={1} lg={2} xl={3}></Col>
                </Row>
            </React.Fragment>
        );
    }
}
