import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import './Success.css';
import tickLogo from '../../assets/icons/tick.png';

export default function Success() {

    function goToHome() {
        window.location.replace("/");
    }

    return (
        <React.Fragment>
            <Row>
                <Col xs={12} sm={12} md={1} lg={2} xl={2}></Col>
                <Col xs={12} sm={12} md={10} lg={8} xl={7} className="ic-success">
                    <div className="center-screen">
                        <img src={tickLogo} alt="ic-success" width="256px" height="256px" />
                        <div className="mgt">
                            <p>Send order complete</p>
                        </div>
                        <div className="mgt">
                            <Button variant="primary" onClick={goToHome}>Continue To Shopping</Button>
                        </div>
                    </div>
                </Col>
                <Col xs={12} sm={12} md={1} lg={2} xl={3}></Col>
            </Row>
        </React.Fragment>
    );
}
