import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectCart, selectProductList, clearCart } from '../../reducers/products';
import { Row, Col, Table, Form, Button } from 'react-bootstrap';
import NumberFormat from 'react-number-format';
import './SummaryOrder.css';
import boxLogo from '../../assets/icons/box.png';
import { useHistory } from 'react-router-dom';

export default function SummaryOrder() {
    const dispatch = useDispatch();
    let history = useHistory();
    const cart = useSelector(selectCart);
    const productList = useSelector(selectProductList);
    const [items, setItems] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);
    const [vat, setVat] = useState(0);
    const [net, setNet] = useState(0);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [address, setAddress] = useState("");

    function onFirstNameChange(e) {
        setFirstName(e.target.value);
    }

    function onLastNameChange(e) {
        setLastName(e.target.value);
    }

    function onEmailChange(e) {
        setEmail(e.target.value);
    }

    function onPhoneNumberChange(e) {
        setPhoneNumber(e.target.value);
    }

    function onAddressChange(e) {
        setAddress(e.target.value);
    }

    function onConfirmOrder() {
        const cf = window.confirm("Do you confirm the send order?");
        if (cf) {
            const orderObj = {
                products: items,
                firstName,
                lastName,
                email,
                phoneNumber,
                address,
                totalPrice,
                vat,
                net
            };

            fetch("http://localhost:3001/api/createOrder", {
                "method": "POST",
                "headers": {
                    "content-type": "application/json",
                    "accept": "application/json"
                },
                "body": JSON.stringify(orderObj)
            })
                .then(response => response.json())
                .then(response => {
                    if (response.success) {
                        dispatch(clearCart());
                        history.push("/success");
                    } else {
                        alert(JSON.stringify(response.error));
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }


    useEffect(() => {
        let itemList = [];
        cart.forEach(el => {
            productList.forEach(product => {
                if (product.id === el.productId) {
                    product = { ...product, qty: el.qty };
                    itemList.push(product);
                }
            });
        });
        let _total = 0;
        let _vat = 0;
        let _net = 0;

        itemList.forEach((item) => {
            _total += (item.price * item.qty);
        });
        _vat = _total * 0.07;
        _net = _total + _vat;
        setTotalPrice(_total);
        setVat(_vat);
        setNet(_net);
        setItems(itemList);
    }, [cart, productList]);

    if (items.length > 0) {
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} sm={12} md={1} lg={2} xl={2}></Col>
                    <Col xs={12} sm={12} md={10} lg={8} xl={7} className="smo-box">
                        <Table responsive>
                            <thead className="center">
                                <tr>
                                    <th>#</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    items.map((item, i) => {
                                        return (
                                            <tr key={i}>
                                                <td className="center no-bd">{i + 1}</td>
                                                <td className="no-bd">{item.name}</td>
                                                <td className="center no-bd">
                                                    <NumberFormat
                                                        decimalScale={2}
                                                        decimalSeparator="."
                                                        fixedDecimalScale
                                                        prefix="฿"
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        value={item.price}
                                                    />
                                                </td>
                                                <td className="center no-bd">
                                                    <NumberFormat
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        value={item.qty}
                                                    />
                                                </td>
                                            </tr>
                                        );
                                    })
                                }
                                <tr>
                                    <td colSpan="2" className="no-bd"></td>
                                    <td className="no-bd txt-right"><b>Total:</b> </td>
                                    <td className="no-bd txt-right"><b>
                                        <NumberFormat
                                            decimalScale={2}
                                            decimalSeparator="."
                                            fixedDecimalScale
                                            prefix="฿"
                                            thousandSeparator={true}
                                            displayType={'text'}
                                            value={totalPrice}
                                        />
                                    </b></td>
                                </tr>
                                <tr>
                                    <td colSpan="2" className="no-bd"></td>
                                    <td className="no-bd txt-right"><b>Vat 7%:</b> </td>
                                    <td className="no-bd txt-right"><b>
                                        <NumberFormat
                                            decimalScale={2}
                                            decimalSeparator="."
                                            fixedDecimalScale
                                            prefix="฿"
                                            thousandSeparator={true}
                                            displayType={'text'}
                                            value={vat}
                                        />
                                    </b></td>
                                </tr>
                                <tr>
                                    <td colSpan="2" className="no-bd"></td>
                                    <td className="no-bd txt-right"><b>Net:</b> </td>
                                    <td className="no-bd txt-right"><b>
                                        <NumberFormat
                                            decimalScale={2}
                                            decimalSeparator="."
                                            fixedDecimalScale
                                            prefix="฿"
                                            thousandSeparator={true}
                                            displayType={'text'}
                                            value={net}
                                        />
                                    </b></td>
                                </tr>
                            </tbody>
                        </Table>

                        <div>
                            <h5>Ship to</h5>
                            <div>
                                <Row>
                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Form.Group controlId="formFirstName">
                                            <Form.Label>First Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={onFirstNameChange} />
                                        </Form.Group>
                                    </Col>
                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Form.Group controlId="formLastName">
                                            <Form.Label>Last Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={onLastNameChange} />
                                        </Form.Group>
                                    </Col>


                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control type="email" placeholder="Enter email" value={email} onChange={onEmailChange} />
                                        </Form.Group>
                                    </Col>
                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Form.Group controlId="formPhoneNumber">
                                            <Form.Label>Phone Number</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Phone Number" value={phoneNumber} onChange={onPhoneNumberChange} />
                                        </Form.Group>
                                    </Col>

                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <Form.Group controlId="exampleForm.ControlAddress">
                                            <Form.Label>Address</Form.Label>
                                            <Form.Control as="textarea" rows={3} vavalue={address} onChange={onAddressChange} />
                                        </Form.Group>
                                    </Col>
                                </Row>

                                <div className="center">
                                    <Button variant="success" onClick={onConfirmOrder}>Confirm Order</Button>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={1} lg={2} xl={3}></Col>
                </Row>
            </React.Fragment>
        )
    } else {
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} sm={12} md={1} lg={2} xl={2}></Col>
                    <Col xs={12} sm={12} md={10} lg={8} xl={7} className="summary-empty">
                        <div className="center-screen">
                            <img src={boxLogo} alt="empty" width="256px" height="256px" />
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={1} lg={2} xl={3}></Col>
                </Row>
            </React.Fragment>
        );
    }
}
