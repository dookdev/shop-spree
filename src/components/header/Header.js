import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { selectQtyItemCart } from '../../reducers/products';
import { Navbar, Badge, Breadcrumb } from 'react-bootstrap';
import './Header.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { useHistory, useLocation } from 'react-router-dom';


export default function Header() {
    const [countItem, setCountItem] = useState(0);
    const [paths, setPaths] = useState([]);
    const qtyItemCart = useSelector(selectQtyItemCart);
    let history = useHistory();
    let location = useLocation();

    useEffect(() => {
        setCountItem(qtyItemCart);
        let splLocation = location.pathname.split("/").filter(path => path !== "");
        splLocation.unshift("");
        setPaths(splLocation);
    }, [qtyItemCart, location]);

    function redirect(path) {
        history.push(path);
    }

    function goToHome() {
        window.location.replace("/");
    }


    return (
        <React.Fragment>
            <Navbar className="color-nav justify-content-between" variant="light">
                <Navbar.Brand onClick={() => { redirect("/") }} className="ic">
                    <img
                        alt=""
                        src="https://react-bootstrap.github.io/logo.svg"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />
      Shop Spree
    </Navbar.Brand>
                <div>
                    <FontAwesomeIcon icon={faShoppingCart} size="lg" className="ic" onClick={() => { redirect("/cart") }} />
                    <Badge variant="success" pill={true} className="ic" onClick={() => { redirect("/cart") }} >{countItem}</Badge>
                </div>

            </Navbar>

            <div>
                <Breadcrumb className="breadcrumb">
                    {
                        paths.map((item, i) => {
                            let isActive = i === (paths.length - 1) ? true : false;
                            if (item === "") {
                                return (<Breadcrumb.Item key={i} active={paths.length === 1} onClick={() => { goToHome() }}>Home</Breadcrumb.Item>);
                            } else {
                                return (<Breadcrumb.Item key={i} active={isActive} onClick={() => { redirect("/" + item) }}>{item}</Breadcrumb.Item>);
                            }
                        })
                    }
                </Breadcrumb>
            </div>
        </React.Fragment>
    )
}
